const path = require('path');
const gulp = require('gulp');
const hb = require('gulp-hb');
const postcss = require('gulp-postcss');
const gulpif = require('gulp-if');
const browserSync = require('browser-sync');
const handlebars = require('gulp-compile-handlebars');
const layouts = require('handlebars-layouts');

const host = browserSync.create();
const paths = {
  source: 'source',
  dest: 'dist',
  styles: ['source/styles/**/*.css', '!source/styles/**/_*.css'],
  scripts: 'source/scripts/**/*.js',
  templates: 'source/templates/*.html',
  partials: 'source/templates/**/*.hbs',
  images: 'source/images/**/*.{jpg,png,gif,svg}',
  fonts: 'source/fonts/*',
  misc: [ 'source/*.{jpg,png,ico,xml,txt}']
};

gulp.task('launch', () => {
  host.init({
    notify: false,
    server: paths.dest,
    reloadDebounce: 400
  });
});

gulp.task('html', (done) => {
  const hbStream = hb()
    .partials(paths.partials)
    .helpers(layouts)
    .data({ foo: 'bar' })

  return gulp.src(paths.templates)
    .pipe(hbStream)
      .on('error', done)
    .pipe(gulp.dest(paths.dest))
    .pipe(host.stream())
});

gulp.task('styles', (done) => {
  var plugins = [
    require("postcss-import")(),
    require("postcss-cssnext")(),
    require('postcss-reporter')
  ];
  return gulp.src(paths.styles)
    .pipe(postcss(plugins))
      .on('error', done)
    .pipe(gulp.dest(`${paths.dest}/assets/css`))
    .pipe(host.stream())
});

gulp.task('scripts', () => {
  return gulp.src(paths.scripts)
    .pipe(gulp.dest(`${paths.dest}/assets/js`))
    .pipe(host.stream())
});

gulp.task('images', () => {
  // process images
  return gulp.src(paths.images)
    .pipe(gulp.dest(`${paths.dest}/assets/img`))
    .pipe(host.stream())
});

gulp.task('fonts', () => {
  // process fonts
  return gulp.src(paths.fonts)
    .pipe(gulp.dest(`${paths.dest}/assets/fonts`))
    .pipe(host.stream())
});

gulp.task('misc', () => {
  // process custom file types
  return gulp.src(paths.misc)
    .pipe(gulp.dest(`${paths.dest}`))
    .pipe(host.stream())
});

gulp.task('watch', () => {
  gulp.watch(paths.templates, ['html']);
  gulp.watch(paths.partials, ['html']);
  gulp.watch(paths.styles[0], ['styles']);
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.fonts, ['fonts']);
  gulp.watch(paths.misc, ['misc']);
});

gulp.task('default', ['html', 'images', 'scripts', 'styles', 'fonts', 'misc']);

gulp.task('serve', ['default', 'watch', 'launch']);
