/* Form Serializer outputs JSON instead of array */
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        var name = this.name.replace(/[\[\]']+/g,'');
        if (o[name]) {
            if (!o[name].push) {
                o[name] = [o[name]];
            }
            o[name].push(this.value || '');
        } else {
            o[name] = [this.value] || '';
        }
    });
    return o;
};

var _debounce = function(fn, wait) {
    var timeout;
    return function() {
        var ctx = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            fn.apply(ctx, args);
        }, wait || 100);
    };
}

// Events Manager
var _events = {
    events: {},
    on: function (eventName, fn) {
        this.events[eventName] = this.events[eventName] || [];
        this.events[eventName].push(fn);
    },
    off: function (eventName, fn) {
        if (this.events[eventName]) {
            for (var i = 0; i < this.events[eventName].length; i++) {
                if (this.events[eventName][i] === fn) {
                    this.events[eventName].splice(i, 1);
                    break;
                }
            };
        }
    },
    trigger: function (eventName, data) {
        if (this.events[eventName]) {
            this.events[eventName].forEach(function (fn) {
                fn(data);
            });
        }
    }
};

// Cookie Handlers
var _cookie = (function () {
    function createCookie(name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
    }

    function readCookie(name) {
        var nameEQ = encodeURIComponent(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name, "", -1);
    }

    return {
        create: createCookie,
        read: readCookie,
        delete: eraseCookie
    }
})();

(function ($, w, console) {
    // Error-free console logs
    function log(message) {
        try { console.info(message); }
        catch (e) { }
        finally { return; }
    }

    function __ytVidId(url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false;
    }

    function initMobileSidebar() {
        if (!$('.nav--sidebar').length) return; // no need to create mobile sidebar if it's not there

        var $sidebarNav = $('.nav--sidebar').clone();
        var mainTitle = $('main h1').html();
        var $mobileSidebar = $('<div/>', { 'class': 'mobile-sidebar' })

        var $mobileSidebarOpen = $('<button/>', { 'class': 'button button--dropdown' })
            .html(mainTitle)
            .on('click', function () {
              $('body').addClass('mobile-sidebar--expanded');
            })

        var $mobileSidebarClose = $('<button/>', { 'class': 'button button--close' })
            .on('click', function () {
              $('body').removeClass('mobile-sidebar--expanded');
            })

        $mobileSidebar
            .append($('<div/>', { 'class': 'mobile-sidebar__head' })
                .append($mobileSidebarOpen)
                .append($mobileSidebarClose)
            )
            .append($('<div/>', { 'class': 'mobile-sidebar__body' }).append($sidebarNav)
            )

        $(w).on('load resize', function () {
            if ($(w).width() <= 992) {
                $mobileSidebar.appendTo('body')
            }
        })
    }

    function initCollapsible() {
        $('.collapsible').each(function (e) {
            var $this = $(this);
            var $target = $this.find('.collapsible__mark')
            var $siblings = $this.siblings('.collapsible')

            $target.on('click', function() {
                if ($this.is('.collapsible--active')) {
                    $this.removeClass('collapsible--active');
                    return;
                }

                $siblings.removeClass('collapsible--active');
                $this.addClass('collapsible--active');
            })
        })
    }

    function initDrawer() {
        var $drawer = $('.drawer');
        var drawerKnobEl = '.drawer__knob';
        var drawerBoxEl = '.drawer__box';
        var speed = 300;

        $drawer.each(function (e) {
            var $this = $(this);
            var $knob = $this.find(drawerKnobEl);
            var $box = $this.find(drawerBoxEl);
            var $label = $knob.find('.icon');

            $this.on('drawer-close', function (e) {
                $label.removeClass('icon--vflipped');
                $box.slideUp(speed);
            });

            $this.on('drawer-open', function (e, scroll) {
                $label.addClass('icon--vflipped');
                $box.slideDown(speed, function () {
                    var headerHeight = $('.header').height();
                    if (!!scroll || scroll === undefined) {
                        $('html,body').animate({ scrollTop: $this.offset().top - headerHeight });
                    }
                });
            });

            // $box.hide();
        });

        $drawer.on('draw', function (e, i) {
            $drawer.not(':eq(' + i + ')').trigger('drawer-close');
            $drawer.eq(i).trigger('drawer-open');
        });

        $drawer.on('click', '.drawer__knob', function (e, i) {
            var $delegate = $(e.delegateTarget);
            var which = $delegate.index('.drawer');
            var opened = $delegate.find('.drawer__box:visible').length;

            if ($delegate.hasClass('cards-set--inline')) {
                if (!$delegate.hasClass('cards-set--list')) {
                    return;
                }
            }

            $(this).trigger('draw', opened ? [] : [which]);
        })
    }

    function initSliders() {
        var slidePaging = function (slider, i) {
            var $image = $(slider.$slides[i]).find('.slide__image')
            var $video = $(slider.$slides[i]).find('.slide__video')
            var src;

            if ($image.length) {
                src = $image.attr('href');
            }

            if ($video.length) {
                src = $video.data('preview');
            }

            return $('<img/>', { 'src': src });
        }

        // only run if slick plugin is loaded
        if (!$.fn.slick) return;

        $('.slides--inline').each(function () {
            var $this = $(this);
            var captions = [];
            var captionClassName = 'slides__detail';
            var defaultCaption = '';
            var $image = $this.find('.slide__image');
            var $video = $this.find('.slide__video');

            // fire only when necessary
            //if ($this.children().length < 2) return;

            if ($image.length) {
                var src = $image.attr('href');
                $image.magnificPopup({ type: 'image' });
            }

            if ($video.length) {
                $video.magnificPopup({
                    type: 'iframe',
                    iframe: {
                        markup: '<div class="mfp-iframe-scaler">' +
                        '<div class="mfp-close"></div>' +
                        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                        '</div>',
                        patterns: {
                            youtube: {
                                index: 'youtube.com/',
                                id: 'v=',
                                src: '//www.youtube.com/embed/%id%?autoplay=1'
                            },
                            youku: {
                                index: 'v.youku.com/',
                                id: function (url) {
                                    var videoId = url.match(/id_(\w+)/);
                                    return videoId[1];
                                },
                                src: '//player.youku.com/embed/%id%'
                            }
                        },
                        srcAction: 'iframe_src',
                    }
                });
            }

            // bind events
            $this
                .on('init breakpoint', function (event, slick) {
                    // build captions list
                    $this.slideCaptions = [];
                    $this.$caption = $('<div/>', { 'class': captionClassName });

                    slick.$slides.each(function (e, el) {
                        var $image = $(el).find('.slide__image');
                        var $video = $(el).find('.slide__video');

                        if ($image.length) {
                            $this.slideCaptions[e] = $image.attr('title') || defaultCaption;
                            $image.css({ backgroundImage: 'url(' + $image.attr('href') + ')' });
                        }

                        if ($video.length) {
                            $this.slideCaptions[e] = $video.attr('title') || defaultCaption;
                            $video.css({ backgroundImage: 'url(' + $video.data('preview') + ')' });
                        }
                    });

                    // fix slick creating clone of $captions
                    if (!slick.$dots) {
                        var index = slick.$slides.index('.' + captionClassName);
                        if (index > -1) {
                            $this.slick('slickRemove', index);
                        }
                        return;
                    }

                    // push caption container on slider
                    $this.append($this.$caption);

                    // inject detail message
                    $this.$caption.html($this.slideCaptions[slick.currentSlide]);
                })
                .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                    $this.$caption.html($this.slideCaptions[nextSlide]);
                });

            var opts = {
                dots: false,
                mobileFirst: true,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        arrows: true,
                        dots: true,
                        fade: true,
                        speed: 800,
                        customPaging: slidePaging
                    }
                }]
            }

            $(this).slick(opts);
        });

        $('.slides--full').each(function () {
            var $this = $(this);
            var $image = $this.find('.slide__image');
            var $video = $this.find('.slide__video');

            if ($(this).children().length < 2) return;

            if ($image.length) {
                var src = $image.attr('href');
                $image.magnificPopup({ type: 'image' });
            }

            if ($video.length) {
                $video.magnificPopup({
                    type: 'iframe',
                    iframe: {
                        markup: '<div class="mfp-iframe-scaler">' +
                        '<div class="mfp-close"></div>' +
                        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                        '</div>',
                        patterns: {
                            youtube: {
                                index: 'youtube.com/',
                                id: 'v=',
                                src: '//www.youtube.com/embed/%id%?autoplay=1'
                            },
                            youku: {
                                index: 'v.youku.com/',
                                id: function (url) {
                                    var videoId = url.match(/id_(\w+)/);
                                    return videoId[1];
                                },
                                src: '//player.youku.com/embed/%id%'
                            }
                        },
                        srcAction: 'iframe_src',
                    }
                });
            }

            $this
                .on('init breakpoint', function (event, slick) {
                    // build captions list
                    slick.$slides.each(function (e, el) {
                        var $image = $(el).find('.slide__image');
                        var $video = $(el).find('.slide__video');

                        if ($image.length) {
                            $image.css({ backgroundImage: 'url(' + $image.attr('href') + ')' });
                        }

                        if ($video.length) {
                            $video.css({ backgroundImage: 'url(' + $video.data('preview') + ')' });
                        }
                    });
                });


            var opt = {
                mobileFirst: true,
                dots: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 5000,
                responsive: [{
                    breakpoint: 991,
                    settings: {
                        speed: 800,
                        fade: true,
                        arrows: true
                    }
                }]
            }

            $(this).slick(opt);
        });

        $('.slides--hero').each(function() {
          var opts = {
            dots: true,
            arrows: false,
            mobileFirst: true,
            autoplay: true,
            autoplaySpeed: 5000,
            responsive: [{
              breakpoint: 991,
              settings: {
                dots: false,
                arrows: true,
              }
            }]
          }

          $(this).slick(opts);
        });

        $('.slides--card').each(function() {
          var $this = $(this);
          var opts = {
            dots: true,
            arrows: false,
            mobileFirst: true,
          }

          $(window).on('load resize', function() {
            if ($(window).width() <= 767) {
              if (!$this.hasClass('slick-initialized')) {
                $this.wrap($('<div/>', { class: 'slide-wrap' }));
                $this.slick(opts);
              }
              return
            }

            if ($this.hasClass('slick-initialized')) {
              $this.unwrap();
              return $this.slick('unslick');
            }
          })
        });

        $('.slides--tale').each(function() {
          var $this = $(this);
          var opts = {
            dots: true,
            arrows: false,
            fade: true,
            mobileFirst: true,
          }

          $(window).on('load resize', function() {
            if ($(window).width() <= 767) {
              if (!$this.hasClass('slick-initialized')) {
                $this.slick(opts);
              }
              return
            }

            if ($this.hasClass('slick-initialized')) {
              return $this.slick('unslick');
            }
          })
        });
      }

    function initCardFilters() {
        $('.card-group').on('click', '.card-group__title', function (e) {
            var $delegate = $(e.delegateTarget);


            if ($delegate.is('.card-group--expanded')) {
                $delegate.removeClass('card-group--expanded');
                return;
            }

            $('.card-group').removeClass('card-group--expanded');

            $delegate.addClass('card-group--expanded');
        })
    }

    function initMultiSelect() {
        var msEl = '.multiselect';
        var msToggleEl = '.multiselect__toggle';
        var msEl = '.multiselect__options';

        $(msEl).each(function () {
            var $this = $(this);
            var $toggle = $this.find(msEl);
        });

    }

    function initTimeDisplay() {
        if (typeof (moment) != 'function') {
            return;
        }

        var serverTime = w.BTTIME || new Date();
        var utcTarget = w.BTTIME_TARGET_UTC || 8;

        var tt = moment(new Date(serverTime)).utcOffset(utcTarget);
        function updateTime(time) {
            $('.data-time-hour').html(time.format('H'));
            $('.data-time-minute').html(time.format('mm'));
        }

        function blinkTime() {
            $('.data-time-separator').fadeIn(1000).fadeOut(1000, blinkTime);
        }

        // update time every second
        setInterval(function () {
            updateTime(tt.add(1, 's'));
        }, 1000);

        // blink animation for time separator
        blinkTime();
    }

    function initWeatherDisplay() {
        if (typeof (moment) != 'function') return;

        var cityId = w.BTOW_CITYID || '1701667'; // Defaults to Manila
        var owApi = w.BTOW_API || '3a8daace8a70e88a932f8da3fb718d26'; // Free API

        $.getJSON('//api.openweathermap.org/data/2.5/weather?id=' + cityId + '&units=metric&appid=' + owApi, function (weather) {
            var icon = weather.weather[0].icon || '01d';

            $('.hero-info .wi, .forecast__1 .wi').attr('class', 'wi').addClass('owm-' + weather.weather[0].icon);
            $('.data-weather, .forecast__1 span').html(weather.main.temp);
        });

        $.getJSON('//api.openweathermap.org/data/2.5/forecast?id=' + cityId + '&units=metric&appid=' + owApi, function (forecast) {
            var today = new Date();
            today.getTime();
            var count = 2;

            $.each(forecast.list, function (e, i) {
                // var date = new Date(i.dt_txt);
                var date = moment(i.dt_txt);

                if (!moment(today).isSame(date, 'd') && date.hour() === 12) {
                    if ($('.forecast__' + count).length) {
                        $('.forecast__' + count).find('.forecast__day')
                            .html(date.format('dddd'));
                        $('.forecast__' + count).find('.forecast__icon .wi')
                            .attr('class', 'wi').addClass('owm-' + i.weather[0].icon);;
                        $('.forecast__' + count).find('span')
                            .html(Math.floor(i.main.temp));
                    }
                    count++
                }
            })

        });
    }

    function initMultiInput() {
        $('.multiinput').each(function () {
            var $this = $(this);
            var expandClass = 'multiinput--expanded';

            $this.on('collapse', function () {
                $(this).removeClass(expandClass);
            })

            $this.on('click', '.multiinput__handle', function (e) {
                var $delegate = $(e.delegateTarget);
                $delegate.addClass(expandClass);
            });

            $(document).on('mouseup', function (e) {
                if (!$this.is(e.target) && $this.has(e.target).length === 0) {
                    $this.trigger('collapse');
                }
            });
        });
    }

    function initTNCHandle() {
        $('.tnc').each(function () {
            var $this = $(this);
            var $handle = $this.find('.tnc__handle');
            var $body = $this.find('.tnc__body');
            var expandedClass = 'tnc--expanded';

            $handle.on('click', function () {
                $this.toggleClass(expandedClass);
            });
        });
    }

    function initVernacular() {
        var $lang = $('.vernacular');
        var host = w.location.host;
        var pathname = w.location.pathname;
        var expire = w.BTLANG_COOKIEEXPIREDAYS || 7;
        var redirect = w.BTLANG_REDIRECT || false;

        $lang.on('open', function () {
            $(this).show();
        });

        $lang.on('close', function () {
            $(this).hide();
        });

        $lang.find('select').on('change', function () {
            var $this = $(this)
            var pathnames = pathname.split('/')

            pathnames.splice(1,1, $this.val())

            $lang.trigger('close');
            setTimeout(function () {
                $(location).attr('href', '//' + host + pathnames.join('/'));
                w._cookie.create('bt_language', $this.val(), expire);
            }, 300);
        });

        $lang.on('click', 'button[type=button]', function (e) {
            $(e.delegateTarget).trigger('close');
            w._cookie.create('bt_language', 'en', expire);
        });

        $('document').ready(function () {
            var paths = pathname.split('/');
            var cookieVal = w._cookie.read('bt_language');

            if (cookieVal) {
                if (paths.indexOf(cookieVal) != -1) return;

                if (redirect) {
                    $(location).attr('href', '//' + host + paths.splice(1,1, cookieVal));
                }

                return;
            }

            setTimeout(function () {
                $lang.show()
            }, 3000);
        });
    }

    function initLanguageSwitch() {
        var $lang = $('.switch-language');
        var $options = $lang.find('select option');
        var $dropdown = $lang.find('.switch-language__dropdown');
        var host = w.location.host;
        var pathname = w.location.pathname;
        var expire = w.BTLANG_COOKIEEXPIREDAYS || 7;

        $options.each(function (i, el) {
          var pathnames = pathname.split('/')
          pathnames.splice(1,1, $(el).val())
          var $el = $('<a/>', { href: '//' + host + pathnames.join('/') }).html($(el).html());

          $el.on('click', function (e) {
            w._cookie.create('bt_language', $(el).val(), expire)
          })
          $dropdown.append($el);
        });

        $(document).on('click', '.switch-language button', function (e) {
          e.preventDefault();
          $lang.toggleClass('switch-language--expanded');
        });

        $(document).mouseup(function(e) {
            var container = $('.switch-language, .switch-language__dropdown');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
              $lang.removeClass('switch-language--expanded');
            }
        });

        $lang.find('select').on('change', function () {
          var $this = $(this)
          var pathnames = pathname.split('/')

          pathnames.splice(1,1, $this.val())

          setTimeout(function () {
              $(location).attr('href', '//' + host + pathnames.join('/'));
              w._cookie.create('bt_language', $this.val(), expire);
          }, 300);
        });
    }

    // On document ready
    $(function () {
        $('#menuOpenButton').on('click', function() {
            $('body').addClass('navigating');
        });

        $('#menuCloseButton').on('click', function() {
            $('body').removeClass('navigating');
        });

        $(document).on('click', '#backToTop', function() {
            $('html,body').animate({ scrollTop: 0 });
        });

        initSliders();
        initCollapsible();
        // initMobileSidebar();
        initDrawer();
        initMultiSelect();
        initMultiInput();
        initTimeDisplay();
        initWeatherDisplay();
        initTNCHandle();
        initVernacular();
        initLanguageSwitch();
    });

})(jQuery, window, window.console);

// List Filter
var _sifter = (function ($, evt) {
    var $filter;
    var targetId;
    var filters = [];

    function init() {
        $filter = $('.sifter__filter');
        targetId = $filter.data('filter-id');

        $('#' + targetId).find('[data-view-category]').each(function () {
            filters.push({ name: $(this).data('view-category'), active: true });
        });

        render();
    }

    function render() {
        var $list = [];

        $.each(filters, function (e) {
            var name = filters[e].name;
            var id = 'category-' + name;
            var $label = $('<label for="' + id + '">' + name + '</label>');
            var $input = $('<input/>', {
                type: 'checkbox',
                name: 'categories[]',
                value: name,
                id: id,
                checked: 'checked'
            }).on('change', toggleCategory);

            $list.push($('<div/>').html($input).append($label));
        });

        $filter.html($list);
    }

    function toggleCategory() {
        var $this = $(this);
        var cat = $this.attr('value');

        $.each(filters, function (index) {
            if (filters[index].name === cat) {
                filters[index].active = $this.is(':checked');
            }
        });

        evt.trigger('filterUpdated', { id: targetId, filters: filters });
    }

    $(function() {
        init();
    })
})(jQuery, _events);

// Filter List
var _sifter_list = (function ($, evt) {
    var filterId;
    var filters;
    var categories = [];
    var $list;

    function init() {
        $list = $('#' + filterId);
        render();
    }

    function render() {
        $.each(filters, function (filter) {
            if (filters[filter].active) {
                $list.find('[data-view-category=' + filters[filter].name + ']').slideDown();
                return;
            }
            $list.find('[data-view-category=' + filters[filter].name + ']').slideUp();
        });
    }

    function updateView(data) {
        $viewer = $('.sifted');
        $set = $viewer.find('.cards-set');

        $set.removeClass('cards-set--list');
        $viewer.find('.drawer__box').show();

        if (data.view === 'list') {
            $set.addClass('cards-set--list');
            $viewer.find('.drawer__box').hide();
        }
    }

    evt.on('filterUpdated', function (data) {
        filterId = data.id;
        filters = data.filters;
        init();
    })

    evt.on('viewChanged', updateView)
})(jQuery, _events);

var _switcher = (function ($, evt) {
    var $switches = $('[data-view-type]');
    var $activeSwitch;
    var viewType;

    var defaults = {
        element: '[data-view-type]',
        eventName: 'viewChanged'
    }

    function init(config) {
       $switches = $(defaults.element)

        evt.on('viewChanged', render);
    }

    function render() {
        $switches.removeClass('button--active');
        $activeSwitch.addClass('button--active');
    }

    function handleClickEvent(view) {
        if (typeof view === 'string') {
            viewType = view
            $activeSwitch = $switches.filter(function (el) {
                return $(this).data('view-type') === view;
            });
        } else {
            $activeSwitch = $(this);
            viewType = $activeSwitch.data('view-type');
        }

        evt.trigger('viewChanged', { id: $activeSwitch.data('view-id'), view: viewType });
    }


    // initialize
    $switches.on('click', handleClickEvent);

    $(function() {
      //switchViewType('grid');
    })

    return {
        switchTo: handleClickEvent
    }
})(jQuery, _events);

var _offers_filter = (function ($, w, evt) {
    var offersList = [];
    var filters;
    var offerTypes = [];
    var targetEl = '#offer_filter_view';
    var $filter;

    function init() {
        $filter = $('#offers_filter');
        targetEl = $filter.data('target-view');

        $(targetEl).find('[data-offer-type]').each(function () {
            var offerType = $(this).data('offer-type').split(',');
            offerTypes = $.unique($.merge(offerTypes, offerType));
        });

        render();
    }

    function render() {
        var $select = $('#offers_types_options');

        $.each(offerTypes, function (i, e) {
            $select.append($('<label/>').append($('<input/>', { type: 'checkbox', name: 'types', value: e, checked: 'checked' })).append(' ' + e));
        });

        $filter.on('submit', onSubmit);
    }

    function onSubmit(e) {
        e.preventDefault();
        evt.trigger('offerFilterUpdated', { id: targetEl, filters: $filter.serializeObject() });
    }

    init();
})(jQuery, window, _events);

var _offers_view = (function ($, w, evt) {
    var filters;
    var targetEl;

    function findOne(haystack, arr) {
        return arr.some(function (v) {
            return haystack.indexOf(v) >= 0;
        });
    };

    function render() {
        var $cards = $(targetEl).find('[data-offer-type]');
        var startDate = new Date(filters['offerStartDate']);

        $cards.each(function (i, card) {
            var $this = $(this);
            var offerType = $(this).data('offer-type');
            var filterDate = filters.offerStartDate[0] ? moment(filters.offerStartDate[0]) : false;
            var startDate = moment($(this).data('start-date'));
            var endDate = moment($(this).data('end-date'));
            var isOfType = findOne(filters.types, offerType.split(','));
            var nullDate = '0001-01-01';

            $this.stop().fadeOut(300);

            if (isOfType) {
                if ( !filterDate || (startDate.isSame(nullDate) && endDate.isSame(nullDate)) ) {
                    $this.stop().fadeIn(300);
                    return;
                }

                if ( endDate.isSame(nullDate) && filterDate.isSameOrAfter(startDate)
                    || ( !endDate.isSame(nullDate) && filterDate.isSameOrAfter(startDate) && filterDate.isSameOrBefore(endDate) )) {
                    $this.stop().fadeIn(300);
                }
            }

        })
    }

    function applyFilter(filt) {
        filters = filt.filters;
        targetEl = filt.id;
        render();
    }

    evt.on('offerFilterUpdated', applyFilter);

})(jQuery, window, _events)

var _location = (function ($, w) {
    function getQuery(query) {
        var vars = query.split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = decodeURIComponent(pair[1]);
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
                query_string[pair[0]] = arr;
            } else {
                query_string[pair[0]].push(decodeURIComponent(pair[1]));
            }
        }
        return query_string;
    }

    return {
        getQuery: getQuery
    }
})(jQuery, window);

var _filterview = (function($, w) {
    var $filter;
    var filterHeight;
    var offsetTop;
    var headerAdjustment;

    function init() {
        $filter = $('.filterview').not('.filterview--compact');
        if (!$filter.length) return; // there isn't aything here

        $toggle = $filter.find('.filterview__toggle button');
        filterHeight = $filter.height();
        offsetTop = $filter.offset().top;
        headerAdjustment = $('.header').outerHeight();

        $toggle.on('click', toggleFilterView)
        $(w).on('scroll', _debounce(watchFilterPosition, 50))
    }

    function watchFilterPosition() {
        var scrollTop = $(w).scrollTop();
        var target = (offsetTop + filterHeight) - headerAdjustment;

        toggleFilterView(scrollTop > target);
    }

    function toggleFilterView(e) {
        if (typeof e === 'object') {
            var collapsed = $filter.hasClass('filterview--collapsed');

            $filter[ collapsed ? 'removeClass': 'addClass' ]('filterview--collapsed');
            return;
        }

        $filter[ e ? 'removeClass': 'addClass' ]('filterview--inline');

    }

    $(function() {
        init();
    });
})(jQuery, window);

var _view_toggle = (function($, w) {
    var $element;
    var view;

    function init(e) {
        $element = $('[data-view-default]');
        view = $element.data('view-default');

        w._events.on('viewChanged', handleViewChange);
    }

    function handleViewChange(e) {
        if (!e) return;
        switchView(e.view);
        view = e.view;
    }

    function switchView(view) {
        var views = {
            grid: 'cards-set--grid',
            detail: 'cards-set--detail'
        }

        Object.keys(views).map(function(v) {
            $element.removeClass(views[v]);
        });

        $element.addClass(views[view]);
    }

    return {
        init: init,
    }
})(jQuery, window);


var _social = (function($, w) {
  function initPin() {
    $.getScript('//assets.pinterest.com/js/pinit.js', function() {
      $(document).on('click', '[data-action=pin]', function(e) {
        e.preventDefault()
        PinUtils.pinAny()
      })
    });
  }

  function initGplus() {
    $(document).on('click', '[data-action=gplus]', function(e) {
      e.preventDefault()
      w.open('https://plus.google.com/share?url=' + w.location.href,
        '',
        'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
      )
    })
  }

  $(function() {
    initPin()
    initGplus()
  })
})(jQuery, window);

var _site_search = (function($, w) {
  function init() {
    $(document).on('click', '#searchButton', function() {
      $('.site-search').addClass('site-search--focused')
        .find('input').focus()
    })

    $(document).mouseup(function(e) {
        var container = $('.site-search');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
          $('.site-search').removeClass('site-search--focused');
        }
    });
  }

  $(function() {
    init()
  })
})(jQuery, window);

var _sidebar = (function($, w) {
  var breakpoint = 992;
  var $openButton;
  var $closeButton;
  var $sidebar;

  function init() {
    $sidebar = $('.nav--sidebar');

    buildToggles()
    buildMenu()

    $(w).on('load resize', render)
  }

  function buildToggles() {
    var title = $('main h1').html();
    $openButton = $('<button>', { class: 'button button--dropdown'}).html(title);

    $(document).on('click', 'main .button--dropdown', function() {
      $('body').addClass('mobile-sidebar--expanded');
    });

    $closeButton = $('<button/>', { class: 'button button--close' });
    $(document).on('click', '.mobile-sidebar .button--close', function() {
      $('body').removeClass('mobile-sidebar--expanded');
    });
  }

  function buildMenu() {
    var nav = $sidebar.clone().removeClass('nav--sidebar');
    $menu = $('<div/>', { class: 'mobile-sidebar' });
    $menu.append($closeButton);
    $menu.append(nav);
  }

  function render() {
    if (!$sidebar.length) return;

    if ($(w).width() > breakpoint) {
      $openButton.remove()
      $menu.remove()
      return;
    }

    if (!$('main').find($openButton).length) {
      $('main').prepend($openButton);
    }

    if (!$('body').find($menu).length) {
      $('body').append($menu)
    }

  }

  return {
    init: init
  }
})(jQuery, window)

$(function() {_sidebar.init(); });

var _footer = (function($, w) {
  var $sticky;

  function init() {
    var $sticky = $('.prefooter__sticky');
    var $origin = $('.prefooter');
    var $root = $('body');
    var fixedClass = 'prefooter--fixed';
    var animClass = 'prefooter--animated';
    var showStart = 0;
    var showEnd;
    var winHeight;
    var scrollTop;

    function showPrefooter() {
      if (scrollTop > showStart && (scrollTop + winHeight) < showEnd) {
        $root.addClass(fixedClass);
      }
    }

    $(w).on('load resize', function() {
      winHeight = $(w).outerHeight();
    });

    $(w).on('scroll', function(e) {
      scrollTop = $(w).scrollTop();
      showEnd = $('.prefooter').offset().top;

      if ($(w).width() < 991) return;

      if ($root.hasClass(fixedClass)) {
        if (scrollTop === showStart || (scrollTop + winHeight) > showEnd) {
          $root.removeClass(fixedClass);
        }
        return;
      }

      showPrefooter();
    });
  }


  return {
    init: init
  }
})(jQuery, window);

$(function() { _footer.init(); });

