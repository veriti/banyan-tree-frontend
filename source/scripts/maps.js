var _data = (function($, w) {
  var data;

  function setData(d, dt) {
    data = { data: d, type: dt }
  }

  function getData() {
    return data;
  }

  return {
    get: getData,
    set: setData
  }
})(jQuery, window);

// View Switcher
var _switch = (function ($, evt) {
  var $switches;
  var $activeSwitch;
  var viewType;
  var initialized;

  function init() {
    $switches = $('[data-view-type]');
    $switches.on('click', setViewType);

    view = $('[data-view-default]').data('view-default');
    setViewType(view);

    render();
    initialized = true
  }

  function render() {
    if (!initialized) return;
    $switches.removeClass('button--active');
    $activeSwitch.addClass('button--active');
  }

  function setViewType(view) {
    if (typeof view === 'string') {
      viewType = view
      $activeSwitch = $switches.filter(function (el) {
        return $(this).data('view-type') === view;
      });
    } else {
      $activeSwitch = $(this);
      viewType = $activeSwitch.data('view-type');
    }

    evt.trigger('viewChanged', { view: viewType });

    return view;
  }

  function getViewType() {
    return viewType;
  }

  function view(type) {
    return type ? setViewType(type) : getViewType()
  }

  evt.on('viewChanged', render);

  return {
    init: init,
    view: view
  }
})(jQuery, _events);

// Filter Form
var _filter = (function($, w, evt) {
  var $form;
  var locations;
  var countries = [];
  var destinations = [];
  var themes = [];
  var delegates = [];

  function init(data) {
    locations = w._data.get().data;
    $form = $('#filter_location');
    $form.on('submit', formSubmit);

    setupData(data);
    render();

    setTimeout(function() {
      $form.trigger('submit');
      $form.find('input').trigger('change');
    }, 100)
  }

  function formSubmit(e) {
    if (typeof e === 'object') {
      e.preventDefault();
    }

    $form = $(this);
    evt.trigger('filterUpdated', $form.serializeObject());
  }

  function render() {
    buildOptions('#countries_option', 'countries', countries);
    buildOptions('#destinations_option', 'destinations', destinations);
    buildOptions('#themes_option', 'themes', themes);
    /* replace options selector */
    buildOptions('[data-options-for="countries"]', 'countries', countries);
    buildOptions('[data-options-for="destinations"]', 'destinations', destinations);
    buildOptions('[data-options-for="themes"]', 'themes', themes);
  }

  function setupData() {
    $.each(locations, function(i, e) {
      if (countries.indexOf(e.country) === -1) {
        countries.push(e.country);
      }

      if (destinations.indexOf(e.destination) === -1) {
        destinations.push(e.destination);
      }

      if (themes.indexOf(e.theme) === -1) {
        themes.push(e.theme);
      }
    });
  }

  function buildOptions(el, fieldName, list) {
    if (list.indexOf(undefined) != -1) return

    var $menu = $form.find(el);
    var $options = [];
    var $label = $('<label/>');
    var hashValue = '';

    if (w.location.hash.length) {
      hashValue = w.location.hash.substr(1).toLowerCase()
    }

    $.each(list, function(i, e) {
      let checked = true;
      var $label = $('<label/>');
      $label.text(' ' + e.charAt(0).toUpperCase() + e.slice(1));

      if (fieldName === 'destinations' || fieldName === 'themes') {
        checked = hashValue && hashValue===e.toLowerCase()
      }

      var $input = $('<input/>', {
        type: 'checkbox',
        name: fieldName,
        value: e,
        checked: checked
      });

      $input.on('change', function() {
        var $ms = $(this).parents('.multiselect');
        var $msLabel = $ms.find('button span');
        var numberChecked = $ms.find('input:checked').length
        var buttonLabel = numberChecked + ' ' + fieldName

        if (numberChecked === $ms.find('input').length) {
          buttonLabel = 'All ' + fieldName
        }

        if (numberChecked === 1) {
          buttonLabel = 'test';
          buttonLabel = $ms.find('input:checked').parent('label').text();
        }

        $msLabel.html(buttonLabel);
      })

      $options.push($label.prepend($input));
    });

    var $selectAll = $('<button/>').text('Select all');

    $menu.html($options);
    // $menu.on('change', function(e) {
    //   var $ms = $(e.target).parents('.multiselect');
    //   $msLabel = $ms.find('button span')
    //   $msLabel.html($ms.find('input:checked').length + ' Selected')
    // })
  }

  return {
    init: init,
    submit: formSubmit
  }
})(jQuery, window, _events);

// Cards
var _cards = (function($, w) {
  var filteredLocations = [];
  var $viewer;
  var view;
  var filters;

  function init(data) {
    locations = w._data.get().data;
    $viewer = $('#location_view');

    view = w._switch.view();

    w._events.on('viewChanged', applyView);
    w._events.on('filterUpdated', applyFilters);
  }

  function applyFilters(f) {
    if (!locations) {
      init();
    }

    filters = f;
    filteredLocations = locations.reduce(reducer, []);

    render();
  }

  function reducer(accumulator, location) {
    if (filters['brands'] && filters['brands'].indexOf(location['brand']) === -1) return accumulator;
    if (filters['countries'] && filters['countries'].indexOf(location['country']) === -1) return accumulator;
    if (filters['destinations'] && filters['destinations'].indexOf(location['destination']) === -1) return accumulator;
    if (filters['themes'] && filters['themes'].indexOf(location['theme']) === -1) return accumulator;

    if (filters['delegates']) {
      var chosenValue = filters['delegates'][0].split(',');
      var cardValue = parseInt(location['features']['delegates']);
      if (chosenValue[0] > cardValue) return accumulator;
      if (chosenValue[1] < cardValue) return accumulator;
    }

    accumulator.push(location);
    return accumulator;
  }

  function render() {
    var countries = [];
    var cardsSet = [];
    var locs = filteredLocations.length ? filteredLocations : w._data.get().data;
    var type = w._data.get().type;

    if (['grid', 'list', 'detail'].indexOf(view) === -1) return;

    var groupedLocations = {};

    locs.map(function(location) {
      if (!groupedLocations[location.country]) {
        groupedLocations[location.country] = [];
      }

      groupedLocations[location.country].push(location);
    });

    Object.keys(groupedLocations).map(function(country) {
      var $set = $('<div class="cards-set cards-set--'+ (type==='wedding' && view==='detail' ? view + '-a' : view) +'"></div>');
      var $setTitle = $('<div class="cards-set__title"><div class="container"><p class="text-left">'+country+'</p></div></div>');
      var $cards = $('<div class="cards"></div>');

      var tHeads = {
        event: $('<div class="cards-thead"><div>Property</div><div>Total Space</div><div>Total Delegates</div><div>Indoor Banquet</div><div>Outdoor Banquet</div><div>Guest Rooms</div><div>Ballroom</div><div>Breakout Rooms</div><div>Seating Style</div></div>'),
        wedding: $('<div class="cards-thead"><div>Property</div><div>Total Space</div><div>Golf</div><div>Beach</div><div>Chapel</div><div>Poolside</div><div>Garden</div><div>Spa</div><div>Activities Center</div><div>Make up Services</div><div>Florists</div></div>'),
        wedding2: $('<div class="cards-thead"><div>Property</div><div>Total Space</div><div><i class="icon icon--seats-a"></i> <strong>Round Table</strong></div><div><i class="icon icon--seats-b"></i><strong>Classroom</strong></div><div><i class="icon icon--seats-c"></i> <strong>Theatre</strong></div><div><i class="icon icon--seats-d"></i><strong>U-shape</strong></div><div><i class="icon icon--seats-e"></i><strong>Boardroom</strong></div><div><i class="icon icon--seats-f"></i><strong>Cocktail</strong></div><div>Others</div></div>')
      }

      groupedLocations[country].map(function(property) {
        var $card = buildCard(property, type);
        $cards.append($card);
      });

      $cards.append('<div class="card card--filler"></div>');
      cardsSet.push($set.append($setTitle).append(tHeads[type]).append($cards));
    });

    $viewer.html(cardsSet);
  }

  function applyView(config) {
    if (!$viewer) {
      init();
    }

    view = config.view;

    render();
  }

  function buildCard(data, type) {
    var desc = data.excerpt;
    if (!/<[a-z][\s\S]*>/i.test(data.excerpt)) { desc = '<p>'+desc+'</p>'; }

    if (type === 'event') {
      var iconset = {
        a: 'roundtable_cap',
        b: 'classroom_cap',
        c: 'theatre_cap',
        d: 'boardroom_cap',
        e: 'ushape_cap',
        f: 'freestand_cap'
      }

      var icons = Object.keys(iconset).map(function(icon) {
        if (data.features) {
          return data.features[iconset[icon]] != '' ? '<i class="icon icon--seats-'+icon+'"></i>':'';
        }
      });

      return '<div class="card"><div class="card__head"><a href="'+data.link+'"><img src="'+data.img+'" alt="'+data.property+'"></a></div><div class="card__body"><h4><a href="'+data.link+'">'+data.property+'</a><a href="'+data.link+'" class="head-link"><i class="icon icon--ui-camera"></i> View photos &gt;</a></h4><hr class="rule rule--centered"><p class="text-primary"><em>'+data.excerpt+'</em></p><ul class="list-tabular"><li><span>'+data.features.size_sqft+'</span>sqft / <span>'+data.features.size_sqm+'</span>m<sup>2</sup></li><li><span>'+data.features.delegates+'</span> Total Delegates</li><li><span>'+data.features.indoor_banquet+'</span> Indoor Banquet</li><li><span>'+data.features.outdoor_banquet+'</span> Outdoor Banquet</li><li><span>'+data.features.guest_rooms+'</span> Guest Rooms</li><li><span>'+data.features.ballroom+'</span> Ballroom</li><li><span>'+data.features.breakout+'</span> Breakout Rooms</li><li class="icons">'+icons.join('')+'</li></ul></div></div>';
    }

    if (type === 'wedding') {
      var iconClass = {};
      Object.keys(data.features).map(function(feature) {
        iconClass[feature] = (data.features[feature]) ? 'check' : 'dash'
      });

      return '<div class="card card--'+data.brand+'"><div class="card__head"><a href="'+data.link+'"><img src="'+data.img+'" alt="'+data.title+'"></a></div><div class="card__body"><h4><a href="'+data.link+'">'+data.title+'</a><a href="'+data.link+'" class="head-link"><i class="icon icon--ui-camera"></i> View photos &gt;</a></h4><hr class="rule rule--centered">'+desc+'<ul class="list-tabular"><li><span>'+data.features.size_sqft+'sqft / '+data.features.size_sqm+'m<sup>2</sup></span></li><li class="item-inline">Golf<i class="icon icon--ui-'+iconClass.golf+'"></i></li><li class="item-inline">Beach<i class="icon icon--ui-'+iconClass.beach+'"></i></li><li class="item-inline">Chapel<i class="icon icon--ui-'+iconClass.chapel+'"></i></li><li class="item-inline">Poolside<i class="icon icon--ui-'+iconClass.poolside+'"></i></li><li class="item-inline">Garden<i class="icon icon--ui-'+iconClass.garden+'"></i></li><li class="item-inline">Spa<i class="icon icon--ui-'+iconClass.spa+'"></i></li><li class="item-inline">Activities Center<i class="icon icon--ui-'+iconClass.activities_center+'"></i></li><li class="item-inline">Make-up Services<i class="icon icon--ui-'+iconClass.makeup+'"></i></li><li class="item-inline">Florists<i class="icon icon--ui-'+iconClass.florist+'"></i></li></ul></div><div class="card__footer"></div></div>';
    }

    return '<div class="card card--'+data.brand+'"><div class="card__head"><a href="'+data.link+'"><img src="'+data.img+'" alt="'+data.title+'"></a></div><div class="card__body"><h4><a href="'+data.link+'">'+data.title+'</a></h4><hr class="rule rule--centered">'+desc+'</div><div class="card__footer"><div class="card__actions"><a href="'+data.booknow+'">Book now &gt;</a><a href="'+data.link+'">View details &gt;</a></div></div></div>';
  }

  return {
    init: init
  }
})(jQuery, window, _events);

var _map = (function ($, w, evt) {
  var $viewer;
  var locations = [];
  var filters;
  var map;
  var mapConfig;
  var clusterSizes;
  var iconPath = 'assets/img/';
  var bounds;
  var markers;
  var markerClusterer;
  var view;
  var $map;
  var $mapContainer;
  var $mapDetails;

  function init(el) {
    $viewer = $('#location_view');
    view = w._switch.view();
    mapConfig = {
      zoom: 3,
      center: { lat: -28.024, lng: 140.887 },
      styles: mapTheme,
      mapTypeControl: false,
      fullscreenControl: false,
      streetViewControl: false
    }
    locations = w._data.get().data;
    clusterSizes = [50, 70, 85, 105, 120];
    iconPath = window.BTMAP_IMAGEPATH;
    markers = [];
    filteredLocations = locations;

    evt.on('filterUpdated', applyFilters);
    evt.on('viewChanged', applyView);
    evt.on('markerClicked', showDetails);
  }

  function onReady() {
    bounds = new google.maps.LatLngBounds();
  }

  function render() {
    if (view != 'map') return;

    if (!$viewer) return;

    $mapDetails = '';
    $mapContainer = $mapContainer || $('<div class="location"></div>');
    $map = $map || $('<div class="map location__map"></div>')

    $mapContainer.removeClass('location--detailed');
    $mapContainer.html($map);
    $mapDetails = $('<div class="location__detail"><button id="closeMapDetail" class="button"><i class="icon icon--ui-close-i"></i></button><header></header><article></article><footer><a href="#"><small>Discover more &gt;</small></a></footer></div>')

    if ($viewer.data('view-details')) {
      $mapContainer.append($mapDetails);
    }

    if ($map.children().length === 0) {
      map = new google.maps.Map($map.get(0), mapConfig);
    }

    $viewer.html($mapContainer);

    clearMarkers();
    setMarkers();
  }

  function setMarkers() {
    var clustererOptions = {
      styles: clusterSizes.map(function (size, i) {
        return {
          url: iconPath + 'm' + (i + 1) + '.png',
          height: size,
          width: size,
          textSize: 20 + (i * 2),
          textColor: 'white'
        }
      })
    }

    filteredLocations.map(function (location, i) {
      var marker = new google.maps.Marker({
        position: location.latLng,
        icon: iconPath + 'pin-' + location.brand + '.png'
      });

      var infoWindow = new google.maps.InfoWindow({
        content: location.title
      });

      marker.addListener('click', onMarkerClicked.bind(marker, location));
      marker.addListener('mouseover', function () {
        infoWindow.open(map, marker);
      });

      marker.addListener('mouseout', function () {
        infoWindow.close(map, marker);
      });

      bounds.extend(marker.getPosition());
      markers.push(marker);

      return marker;
    });

    markerClusterer = new MarkerClusterer(map, markers, clustererOptions);
    map.fitBounds(bounds);
    google.maps.event.trigger(map, 'resize');
  }

  function clearMarkers() {
    markers = [];
    markerClusterer && markerClusterer.clearMarkers();
  }

  function onMarkerClicked(location) {
    evt.trigger('markerClicked', location);
    google.maps.event.trigger(map, 'resize');
    map.panTo(this.getPosition());
    map.setZoom(14);
  }

  function applyFilters(f) {
    filters = f;

    filteredLocations = locations.reduce(reducer, []);

    render();
  }

  function applyView(config) {
    if (config) {
      view = config.view;
    }

    var poll = setInterval(function () {
      if (!google) {
        init();
        clearInterval(poll);
        return;
      }
    }, 1000);

    render();
  }

  function reducer(accumulator, location) {
    if (filters['brands'] && filters['brands'].indexOf(location['brand']) === -1) return accumulator;
    if (filters['countries'] && filters['countries'].indexOf(location['country']) === -1) return accumulator;
    if (filters['themes'] && filters['themes'].indexOf(location['theme']) === -1) return accumulator;
    if (filters['destinations'] && filters['destinations'].indexOf(location['destination']) === -1) return accumulator;

    accumulator.push(location);
    return accumulator;
  }

  function showDetails(location) {
    var $body = $('.location__detail article');
    var $head = $('.location__detail header');
    var $link = $('.location__detail footer a');
    var type = _data.get().type;
    var bodyHtml = '';

    $head.html($('<img/>', { src: location.img }));

    bodyHtml += location.brand && '<i class="icon icon--brand-' + location.brand + '-i"></i>';
    bodyHtml += '<h2>' + location.title + '</h2>';
    bodyHtml += location.description ? '<p>'+location.description+'</p>' : '<p>'+location.excerpt+'</p>';

    if (type === 'event') {
      var detailsHtml = '';
      detailsHtml += '<ul>'
      detailsHtml += '<li>'+location.features.size_sqft+'sqft / '+ location.features.size_sqm+' m<sup>2</sup></li>';
      detailsHtml += '<li>'+location.features.delegates+' Total Delegates</li>';
      detailsHtml += '<li>'+location.features.indoor_banquet+' Indoor Banquet</li>';
      detailsHtml += '<li>'+location.features.outdoor_banquet+' Outdoor Banquet</li>';
      detailsHtml += '<li>'+location.features.guest_rooms+' Guest Rooms</li>';
      detailsHtml += '<li>'+location.features.ballroom+' Ballroom</li>';
      detailsHtml += '<li>'+location.features.breakout+' Breakout Rooms</li>';
      detailsHtml += '<li class="list-inlne">';
      detailsHtml += location.features.roundtable_cap ? '<i class="icon icon--seats-a"></i>': '';
      detailsHtml += location.features.classroom_cap ? '<i class="icon icon--seats-b"></i>': '';
      detailsHtml += location.features.boardroom_cap ? '<i class="icon icon--seats-c"></i>': '';
      detailsHtml += location.features.freestand_cap ? '<i class="icon icon--seats-d"></i>': '';
      detailsHtml += location.features.theatre_cap ? '<i class="icon icon--seats-e"></i>': '';
      detailsHtml += location.features.ushape_cap ? '<i class="icon icon--seats-b"></i>': '';
      detailsHtml += '</li>'
      detailsHtml += '</ul>'
      bodyHtml += detailsHtml
    }

    if (type === 'wedding') {
      var detailsHtml = '';
      var detailsArray = [];

      location.features.golf && detailsArray.push('<span>Golf</span>');
      location.features.beach && detailsArray.push('<span>Beach</span>');
      location.features.chapel && detailsArray.push('<span>Chapel</span>');
      location.features.poolside && detailsArray.push('<span>Poolside</span>');
      location.features.spa && detailsArray.push('<span>Spa</span>');
      location.features.activities_center && detailsArray.push('<span>Activities Center</span>');
      location.features.makeup && detailsArray.push('<span>Make-up Services</span>');
      location.features.florists && detailsArray.push('<span>Florists</span>');

      detailsHtml += '<ul>';
      detailsHtml += detailsArray.join(', ');
      detailsHtml += '<li class="list-inlne">';
      detailsHtml += '</li>'
      detailsHtml += '</ul>';
      bodyHtml += detailsHtml
    }

    $body.html(bodyHtml);
    $link.attr('href', location.link);

    $('.location').addClass('location--detailed');

    $('#closeMapDetail').on('click', hideDetails);
  }

  function hideDetails() {
    $('.location').removeClass('location--detailed');
    google.maps.event.trigger(map, 'resize');
  }

  return {
    init: init,
    onReady: onReady,
    filter: applyFilters
  }

})(jQuery, window, _events);

var mapTheme = [ { "elementType": "geometry", "stylers": [ { "color": "#f5f5f5" } ] }, { "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "elementType": "labels.text.fill", "stylers": [ { "color": "#6da485" } ] }, { "elementType": "labels.text.stroke", "stylers": [ { "color": "#f5f5f5" } ] }, { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [ { "color": "#bdbdbd" } ] }, { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#eeeeee" } ] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#e5e5e5" } ] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] }, { "featureType": "road", "elementType": "geometry", "stylers": [ { "color": "#ffffff" } ] }, { "featureType": "road.arterial", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [ { "color": "#dadada" } ] }, { "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [ { "color": "#e5e5e5" } ] }, { "featureType": "transit.station", "elementType": "geometry", "stylers": [ { "color": "#eeeeee" } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#c1dece" } ] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [ { "color": "#66a07e" } ] } ];

/* On Map Load */
function onGMapLoaded() {
  window._events.trigger('mapLoaded');
  _map.onReady();
  _events.trigger('viewChanged', { view: $('[data-view-type-default]').data('view-type-default') });
}
