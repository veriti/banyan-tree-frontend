# TODO

### Global Templates

  - [ ] bestPrice_form
  - [ ] bestPrice
  - [ ] edm
  - [ ] event_findavenus
  - [ ] event_package
  - [ ] event
  - [ ] findahotel
  - [ ] gifting
  - [x] landingpage_global
  - [x] offer_details
  - [x] offer
  - [ ] opening_C
  - [ ] pressrelease
  - [ ] residences
  - [ ] spa_brandstory
  - [ ] spa_signature2
  - [ ] spa
  - [ ] upcominghotel
  - [ ] wedding_intimateproposal
  - [ ] wedding_location
  - [ ] wedding

### Property Templates

  - [ ] activities
  - [ ] dining_form
  - [ ] dining_thankyou
  - [ ] dining
  - [ ] event_detail
  - [ ] event
  - [ ] landingpage
  - [ ] location
  - [ ] offer_advance
  - [ ] offer_creditgoesto
  - [ ] offer
  - [x] room_details
  - [x] room_villa
  - [ ] spa_form
  - [ ] spa_signature
  - [ ] spa_wellness
  - [ ] spa
  - [ ] upcominghotel
  - [ ] wedding_detail_A

