# README

### Development

*Requirements* 

- Terminal / Command Prompt
- NodeJS
- Text Editor

*Installation*

Just like any regular node application, you can bootstrap the working files by launching your terminal and navigating to the cloned folder then run the following command. 

```
npm install
```

*Start Developing*

Once installation is complete, you can simply run the development build using the command that follows. This will launch a node server (defaults to http://localhost:3000) that'll host the development website. Editing the source files will automatically process your updates and refresh the browser as you develop.

```
npm run serve
```

### Build

Once satisfied with the preview, you can proceed to generate the production files by simply running the following command. This will generate the production files onto the folder `dist`.

```
gulp
```
